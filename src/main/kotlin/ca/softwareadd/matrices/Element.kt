package ca.softwareadd.matrices

interface Element<T : Number> {

    operator fun div(value: T): Element<T>

    operator fun div(value: Element<T>): Element<T> = div(value.value)

    operator fun minus(value: T): Element<T>

    operator fun minus(value: Element<T>): Element<T> = minus(value.value)

    operator fun plus(value: T): Element<T>

    operator fun plus(value: Element<T>): Element<T> = plus(value.value)

    fun reciprocal(): Element<T>

    operator fun rem(value: T): Element<T>

    operator fun rem(value: Element<T>): Element<T> = rem(value.value)

    operator fun times(value: T): Element<T>

    operator fun times(value: Element<T>): Element<T> = times(value.value)

    operator fun unaryMinus(): Element<T>

    val value: T
}

data class IntElement(override val value: Int) : Element<Int> {

    override fun div(value: Int): Element<Int> = IntElement(this.value / value)

    override fun minus(value: Int): Element<Int> = IntElement(this.value - value)

    override fun plus(value: Int): Element<Int> = IntElement(this.value + value)

    override fun reciprocal(): Element<Int> {
        throw UnsupportedOperationException()
    }

    override fun rem(value: Int): Element<Int> = IntElement(this.value % value)

    override fun times(value: Int): Element<Int> = IntElement(this.value * value)

    override fun unaryMinus(): Element<Int> = IntElement(-value)
}

data class FractionElement(override val value: Fraction) : Element<Fraction> {

    override fun div(value: Fraction): Element<Fraction> = FractionElement(this.value / value)

    override fun minus(value: Fraction): Element<Fraction> = FractionElement(this.value - value)

    override fun plus(value: Fraction): Element<Fraction> = FractionElement(this.value + value)

    override fun reciprocal(): Element<Fraction> = FractionElement(this.value.reciprocal())

    override fun rem(value: Fraction): Element<Fraction> {
        throw UnsupportedOperationException()
    }

    override fun times(value: Fraction): Element<Fraction> = FractionElement(this.value * value)

    override fun unaryMinus(): Element<Fraction> = FractionElement(-value)
}