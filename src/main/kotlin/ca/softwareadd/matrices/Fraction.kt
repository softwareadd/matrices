package ca.softwareadd.matrices

import java.util.*
import kotlin.math.absoluteValue

/**
 *
 * @author Eugene Ossipov
 */

fun greatestCommonDivisor(value1: Int, value2: Int): Int {
    require(value1 != 0)
    require(value2 != 0)

    var pair = if (value1 > value2) value1 to value2 else value2 to value1

    while (pair.second != 0) pair = pair.second to (pair.first % pair.second)
    return pair.first
}

fun leastCommonMultiple(value1: Int, value2: Int): Int =
        (value1 * value2).absoluteValue / greatestCommonDivisor(value1, value2)

infix fun Int.over(other: Int) = Fraction(this, other)

operator fun Int.div(other: Fraction) = other.reciprocal() + this

operator fun Int.minus(other: Fraction) =  -other + this

operator fun Int.plus(other: Fraction) = other + this

operator fun Int.times(other: Fraction) = other * this

data class Fraction(val numerator: Int, val denominator: Int = 1) : Number(), Comparable<Fraction> {

    init {
        require(denominator != 0)
    }

    override fun compareTo(other: Fraction): Int {
        val lcm = leastCommonMultiple(this.denominator, other.denominator)
        return (this.numerator * lcm / this.denominator).compareTo(other.numerator * lcm / other.denominator)
    }

    operator fun div(other: Fraction) = times(Fraction(other.denominator, other.numerator))

    operator fun div(other: Int) = div(Fraction(other))

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Fraction

        return compareTo(other) == 0
    }

    private fun fraction(numerator: Int, denominator: Int): Fraction {
        require(denominator != 0)
        if (numerator == 0) return Fraction(0)
        val gcd = greatestCommonDivisor(denominator, numerator)
        return Fraction(numerator / gcd, denominator / gcd)
    }

    override fun hashCode(): Int = fraction(numerator, denominator).run { Objects.hash(numerator, denominator) }

    operator fun minus(other: Fraction) = plus(-other)

    operator fun minus(other: Int) = minus(Fraction(other))

    operator fun plus(other: Fraction): Fraction {
        val lcm = leastCommonMultiple(this.denominator, other.denominator)
        val numerator = this.numerator * lcm / this.denominator + other.numerator * lcm / other.denominator
        return fraction(numerator, lcm)
    }

    operator fun plus(other: Int) = plus(Fraction(other))

    fun reciprocal() = Fraction(denominator, numerator)

    operator fun times(other: Fraction) = fraction(numerator * other.numerator, denominator * other.denominator)

    operator fun times(other: Int) = times(Fraction(other))

    override fun toByte(): Byte = toInt().toByte()

    override fun toChar(): Char = toInt().toChar()

    override fun toDouble(): Double = numerator.toDouble() / denominator.toDouble()

    override fun toFloat(): Float = toDouble().toFloat()

    override fun toInt(): Int = numerator / denominator

    override fun toLong(): Long = toInt().toLong()

    override fun toShort(): Short = toInt().toShort()

    override fun toString(): String = "$numerator/$denominator"

    operator fun unaryMinus(): Fraction = Fraction(-numerator, denominator)
}