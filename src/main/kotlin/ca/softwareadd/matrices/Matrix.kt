package ca.softwareadd.matrices

import java.util.*

/**
 *
 * @author Eugene Ossipov
 */
fun <T : Number> matrix(values: Array<Array<T>>,
                        element: (value: T) -> Element<T>): Matrix<T> {
    require(values.isNotEmpty())
    require(values[0].isNotEmpty())

    val rows = values.size
    val cols = values[0].size

    require(values.all { it.size == cols })

    return Matrix(values.flatMap { it.asIterable() }.map(element), rows, cols)
}

fun intMatrix(values: Array<Array<Int>>): Matrix<Int> = matrix(values, ::IntElement)

class Matrix<T : Number>(
        val values: List<Element<T>>,
        val rows: Int,
        val cols: Int
) {

    init {
        require(rows > 0) { "Number of rows must be positive." }
        require(cols > 0) { "Number of columns must be positive." }
        require(values.size == (rows * cols)) { "Number of elements does not match the dimensions." }
    }

    private fun cofactor(row: Int, col: Int): Element<T> {
        val det = submatrix(row, col).internalDet()
        return if ((row + col) % 2 == 0) det else -det
    }

    fun det(): T {
        require(cols == rows) { "The operation supported for square matrices only." }
        return internalDet().value
    }

    operator fun div(other: T): Matrix<T>  = Matrix(values.map { it / other }, rows, cols)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Matrix<*>

        if (values != other.values) return false
        if (rows != other.rows) return false
        if (cols != other.cols) return false

        return true
    }

    override fun hashCode(): Int = Objects.hash(values, rows, cols)

    operator fun get(row: Int, col: Int): T {
        require(row in 0 until rows) { "The row $row is out of bound." }
        require(col in 0 until cols) { "The col $col is out of bound." }
        return values[index(row, col)].value
    }

    private fun index(row: Int, col: Int) = row * cols + col

    private fun internalDet(): Element<T> =
            when (rows) {
                1 -> values[0]
                2 -> values[0] * values[3] - values[1] * values[2]
                else -> {
                    (0 until cols).map { col ->
                        values[index(0, col)] * cofactor(0, col)
                    }.reduce { acc, element -> acc + element }
                }
            }

    fun inversion(): Matrix<T> {
        val list = (0 until rows).flatMap { row -> (0 until cols).map { col -> row to col } }
                .map { (row, col) -> cofactor(row, col) }
        return Matrix(list, rows, cols).transpose() / internalDet().value
    }

    operator fun minus(other: Matrix<T>): Matrix<T> {
        require(rows == other.rows) { "The number of rows must be the same: $rows <> ${other.rows}" }
        require(cols == other.cols) { "The number of cols must be the same: $cols <> ${other.cols}" }

        return Matrix(values.zip(other.values) { a, b -> a - b }, rows, cols)
    }

    operator fun plus(other: Matrix<T>): Matrix<T> {
        require(rows == other.rows) { "The number of rows must be the same: $rows <> ${other.rows}" }
        require(cols == other.cols) { "The number of cols must be the same: $cols <> ${other.cols}" }

        return Matrix(values.zip(other.values) { a, b -> a + b }, rows, cols)
    }

    fun submatrix(rowToRemove: Int, colToRemove: Int): Matrix<T> {
        require(rowToRemove in 0 until rows) { "The row $rowToRemove is out of bound." }
        require(colToRemove in 0 until cols) { "The col $colToRemove is out of bound." }
        require(rows > 1 || cols > 1)
        val result = (0 until rows).asSequence().filter { it != rowToRemove }
                .flatMap { row -> (0 until cols).asSequence().filter { it != colToRemove }.map { col -> row to col } }
                .map { (row, col) -> values[index(row, col)] }
                .toList()
        return Matrix(result, rows - 1, cols - 1)
    }

    operator fun times(other: T): Matrix<T> = Matrix(values.map { it * other }, rows, cols)

    operator fun times(other: Matrix<T>): Matrix<T> {
        require(cols == other.rows)
        val result = (0 until rows).flatMap { row -> (0 until other.cols).map { col -> row to col } }
                .map { (row, col) ->
                    val thisRow = (0 until cols).asSequence().map { values[index(row, it)] }
                    val otherCol = (0 until other.rows).asSequence().map { other.values[other.index(it, col)] }
                    thisRow.zip(otherCol) { a, b -> a * b }
                            .reduce { acc, element -> acc + element }
                }
        return Matrix(result, rows, other.cols)
    }

    override fun toString(): String =
            (0 until rows).joinToString("\n") { row ->
                (0 until cols).map { col -> values[index(row, col)].value }.joinToString("\t", "\t")
            }

    fun transpose(): Matrix<T> {
        val transpose =
                (0 until cols).flatMap { col -> (0 until rows).map { row -> row to col } }
                        .map { (row, col) -> values[index(row, col)] }

        return Matrix(transpose, cols, rows)
    }
}
