package ca.softwareadd.matrices

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

/**
 * @author Eugene Ossipov
 */
internal class FractionTest {

    @Test
    internal fun compareTo() {
        assertTrue((1 over 4) > (1 over 6))
        assertTrue((1 over 4) < (2 over 6))
    }

    @Test
    internal fun testEquals() {
        assertTrue((2 over 4) == (3 over 6))
        assertTrue((1 over 4) == (1 over 4))
        assertFalse((1 over 4) == (1 over 6))
    }

    @Test
    internal fun testHashCode() {
        assertTrue((2 over 4).hashCode() == (3 over 6).hashCode())
        assertTrue((1 over 4).hashCode() == (1 over 4).hashCode())
        assertFalse((1 over 4).hashCode() == (1 over 6).hashCode())
    }

    @Test
    internal fun plus() {
        with((1 over 4) + (1 over 6)) {
            assertEquals(5, numerator)
            assertEquals(12, denominator)
        }

        with((2 over 4) + (2 over 6)) {
            assertEquals(5, numerator)
            assertEquals(6, denominator)
        }

        with((0 over 4) + (1 over 6)) {
            assertEquals(1, numerator)
            assertEquals(6, denominator)
        }
    }

    @Test
    internal fun testToString() {
        assertEquals("1/4", (1 over 4).toString())
    }
}