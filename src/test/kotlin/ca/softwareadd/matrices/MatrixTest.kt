package ca.softwareadd.matrices

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test

/**
 * @author Eugene Ossipov
 */
internal class MatrixTest {

    @Test
    internal fun ctor() {
        val matrix = intMatrix(arrayOf(
                arrayOf(1, 2, 3),
                arrayOf(4, 5, 6)
        ))
        assertEquals(2, matrix.rows)
        assertEquals(3, matrix.cols)
        (1..6).forEach { assertEquals(it, matrix.values[it - 1].value) }
    }

    @Test
    internal fun ctor_empty() {
        assertThrows(IllegalArgumentException::class.java) { intMatrix(arrayOf()) }
    }

    @Test
    internal fun ctor_empty_1() {
        assertThrows(IllegalArgumentException::class.java) { intMatrix(arrayOf(arrayOf())) }
    }

    @Test
    internal fun ctor_not_aligned() {
        assertThrows(IllegalArgumentException::class.java) {
            intMatrix(arrayOf(
                    arrayOf(1, 2),
                    arrayOf(3, 4, 5)
            ))
        }
    }

    @Test
    internal fun ctor_not_aligned_1() {
        assertThrows(IllegalArgumentException::class.java) {
            Matrix(listOf(1, 2, 3, 4, 5).map(::IntElement), 2, 3)
        }
    }

    @Test
    internal fun det() {
        assertEquals(69, intMatrix(arrayOf(
                arrayOf(11, -2),
                arrayOf(7, 5)
        )).det())

        assertEquals(0, intMatrix(arrayOf(
                arrayOf(1, 2, 3),
                arrayOf(4, 5, 6),
                arrayOf(7, 8, 9)
        )).det())

        assertEquals(-80, intMatrix(arrayOf(
                arrayOf(-2, 1, 3, 2),
                arrayOf(3, 0, -1, 2),
                arrayOf(-5, 2, 3, 0),
                arrayOf(4, -1, 2, -3)
        )).det())
    }

    @Test
    internal fun get() {
        val matrix = intMatrix(arrayOf(
                arrayOf(1, 2, 3),
                arrayOf(4, 5, 6)
        ))
        assertEquals(1, matrix[0, 0])
        assertEquals(2, matrix[0, 1])
        assertEquals(3, matrix[0, 2])
        assertEquals(4, matrix[1, 0])
        assertEquals(5, matrix[1, 1])
        assertEquals(6, matrix[1, 2])
    }

    @Test
    internal fun get_illegal() {
        val matrix = intMatrix(arrayOf(
                arrayOf(1, 2, 3),
                arrayOf(4, 5, 6)
        ))
        assertThrows(IllegalArgumentException::class.java) { matrix[-1, 0] }
        assertThrows(IllegalArgumentException::class.java) { matrix[2, 0] }
        assertThrows(IllegalArgumentException::class.java) { matrix[0, -1] }
        assertThrows(IllegalArgumentException::class.java) { matrix[0, 3] }
    }

    @Test
    internal fun inversion() {
        val expected = intMatrix(arrayOf(
                arrayOf(1, -1, 1),
                arrayOf(-38, 41, -34),
                arrayOf(27, -29, 24)
        ))

        val actual = intMatrix(arrayOf(
                arrayOf(2, 5, 7),
                arrayOf(6, 3, 4),
                arrayOf(5, -2, -3)
        )).inversion()
        assertEquals(expected, actual)
    }

    @Test
    internal fun plus() {
        val expected = intMatrix(arrayOf(
                arrayOf(5, 7, 9),
                arrayOf(11, 13, 15)
        ))
        val matrix1 = intMatrix(arrayOf(
                arrayOf(1, 2, 3),
                arrayOf(4, 5, 6)
        ))
        val matrix2 = intMatrix(arrayOf(
                arrayOf(4, 5, 6),
                arrayOf(7, 8, 9)
        ))

        val actual = matrix1 + matrix2
        assertEquals(expected, actual)
    }

    @Test
    internal fun plus_invalid_size() {
        val matrix1 = intMatrix(arrayOf(
                arrayOf(1, 2, 3),
                arrayOf(4, 5, 6)
        ))
        val matrix2 = intMatrix(arrayOf(
                arrayOf(4, 5),
                arrayOf(7, 8)
        ))
        assertThrows(IllegalArgumentException::class.java) { matrix1 + matrix2 }
    }

    @Test
    internal fun submatrix() {
        val expected = intMatrix(arrayOf(
                arrayOf(1, 3, 4),
                arrayOf(5, 7, 8)
        ))
        val matrix = intMatrix(arrayOf(
                arrayOf(1, 2, 3, 4),
                arrayOf(5, 6, 7, 8),
                arrayOf(9, 10, 11, 12)
        ))

        val actual = matrix.submatrix(2, 1)
        assertEquals(expected, actual)

        assertThrows(IllegalArgumentException::class.java) { matrix.submatrix(3, 1) }
        assertThrows(IllegalArgumentException::class.java) { matrix.submatrix(-1, 1) }
        assertThrows(IllegalArgumentException::class.java) { matrix.submatrix(1, -1) }
        assertThrows(IllegalArgumentException::class.java) { matrix.submatrix(1, 4) }
        assertThrows(IllegalArgumentException::class.java) { intMatrix(arrayOf(arrayOf(1))).submatrix(0, 0) }
    }

    @Test
    internal fun testToString() {
        val expected = "\t1\t2\t3\n\t4\t5\t6"
        val matrix = intMatrix(arrayOf(
                arrayOf(1, 2, 3),
                arrayOf(4, 5, 6)
        ))
        assertEquals(expected, matrix.toString())
    }

    @Test
    internal fun times_scalar() {
        val expected = intMatrix(arrayOf(
                arrayOf(2, 16, -6),
                arrayOf(8, -4, 10)
        ))
        val matrix = intMatrix(arrayOf(
                arrayOf(1, 8, -3),
                arrayOf(4, -2, 5)
        ))

        val actual = matrix * 2
        assertEquals(expected, actual)
    }

    @Test
    internal fun times_matrix() {
        val expected = intMatrix(arrayOf(
                arrayOf(-1, 1),
                arrayOf(2, 2),
                arrayOf(3, 3)
        ))
        val matrix1 = intMatrix(arrayOf(
                arrayOf(1, -1),
                arrayOf(2, 0),
                arrayOf(3, 0)
        ))
        val matrix2 = intMatrix(arrayOf(
                arrayOf(1, 1),
                arrayOf(2, 0)
        ))
        val actual = matrix1 * matrix2
        assertEquals(expected, actual)
    }

    @Test
    internal fun times_matrix_invalid() {
        val matrix1 = intMatrix(arrayOf(
                arrayOf(1, 2, 3),
                arrayOf(4, 5, 6)
        ))
        val matrix2 = intMatrix(arrayOf(
                arrayOf(4, 5),
                arrayOf(7, 8)
        ))
        assertThrows(IllegalArgumentException::class.java) { matrix1 * matrix2 }
    }

    @Test
    internal fun transpose() {
        val matrix = intMatrix(arrayOf(
                arrayOf(1, 2, 3),
                arrayOf(4, 5, 6)
        ))

        val expected = intMatrix(arrayOf(
                arrayOf(1, 4),
                arrayOf(2, 5),
                arrayOf(3, 6)
        ))

        val actual = matrix.transpose()

        assertEquals(expected, actual)
    }
}